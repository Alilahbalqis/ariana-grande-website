# COMPONENTS

**This directory is not required, you can delete it if you don't want to use it.**

The components directory contains your Vue.js Components.

_Nuxt.js doesn't supercharge these components._

Penjelasan:

- folder `components` ini berisi dengan component yang digunakan di aplikasi web. Pada saat ini hanya ada component `Navbar` saja
