# LAYOUTS

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your Application Layouts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/views#layouts).

Penjelasan:

- Terdiri dari dua layout yaitu layout yang digunakan pada saat user manager telah login `dashboard.vue` dan layout yang digunakan tanpa login `default.vue`
- Pada layout `default.vue` terdiri dari `navbar`, `footer`, dan `view` yang dinamis tergantung dari `pages` mana. Misal: url yang diakses saat ini `/about` berarti `view` yang tampil merupakan dari file `about.vue`
- Pada layout `dashboard.vue` terdiri dari `navbar`, `sidebar` dan `view`
