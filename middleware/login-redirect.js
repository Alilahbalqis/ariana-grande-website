export default function ({ store, redirect }) {
  // If the user is authenticated
  if (store.state.token && store.state.user) {
    return redirect('/dashboard')
  }
}
