export default function ({ store, redirect }) {
  // If the user is not authenticated
  if (!store.state.token || !store.state.user) {
    store.dispatch('clearUserAndToken')
    return redirect('/')
  }
}
