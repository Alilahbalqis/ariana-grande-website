# PLUGINS

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains Javascript plugins that you want to run before mounting the root Vue.js application.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/plugins).

Penjelasan

- Plugin disini digunakan untuk mengatur axios untuk berhubungan dengan API pada file `axios.js`
- `vuex-init.js` digunakan untuk mengatur store/vuex terutama untuk localStorage
