export default ({ store }) => {
  const userData = JSON.parse(localStorage.getItem('user')) || null
  store.commit('SET_USER', userData)

  const tokenData = JSON.parse(localStorage.getItem('token')) || null

  store.commit('SET_TOKEN', tokenData)
  store.dispatch('setAxios')
}
