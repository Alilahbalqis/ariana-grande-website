# PAGES

This directory contains your Application Views and Routes.
The framework reads all the `*.vue` files inside this directory and creates the router of your application.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/routing).

Penjelasan:

- Pada folder `pages` ini berisi file yang sekaligus menjadi halaman dengan route sesuai nama file untuk aplikasi web. Misal: file `login.vue` akan menghasilkan url: `/login` beserta halaman login di dalamnya. Khusus file `index.vue` akan menjadi root url/url utama saat membuka web url tersebut berupa `/`.
- Selain file di folder `pages` ini juga berisi folder. Folder ini digunakan apabila ingin membuat nested route. Misal: news diawali dengan halaman yang berisi list berita dengan url `/news`, lalu ada halaman detail berita misal `/news/1`. Dalam kasus seperti contoh tersebut maka perlu dibuat folder news yang didalamnya ada file `index.vue` untuk halaman list berita dan nanti akan menjadi url `/news`, lalu file `_id.vue` ini akan menjadi halaman detail berita url `/news/1` (`1` pada url disini merupakan id detail berita). `_id.vue` ini dinamis dalam artian konten dalam halaman tersebut dapat menyesuaikan isi berita sesuai id berita.
